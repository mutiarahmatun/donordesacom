from django.apps import AppConfig


class DonordesaConfig(AppConfig):
    name = 'donordesa'
