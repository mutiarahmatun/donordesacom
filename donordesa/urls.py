from django.urls import path
from donordesa import views

app_name = 'donordesa'
urlpatterns = [
	path('', views.home, name='home'),
	path('home', views.home, name='home'),
]
