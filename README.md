# DonorDesa.com
## Authors
* **Mutia Rahmatun Husna**
* **Nurfadhilah Ginting**
* **Muhammad Badrul Munir** 
* **Firman Siregar** 
* **Supriatna**

## Links
* Herokuapp: https://donordesa.herokuapp.com/

## Pipeline Status
[![pipeline status](https://gitlab.com/mutiarahmatun/donordesacom/badges/master/pipeline.svg)](https://gitlab.com/mutiarahmatun/donordesacom/commits/master)

## Coverage Report
[![coverage report](https://gitlab.com/mutiarahmatun/donordesacom/badges/master/coverage.svg)](https://gitlab.com/mutiarahmatun/donordesacom/commits/master)
